/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.melonsoft.tictactoe;

import java.util.HashMap;

/**
 *
 * @author ArbuzovA
 */
public class Field {

    public void ShowField(int cellNum, String symb, HashMap cells) {
        
        //update hasmap
        cells.put(cellNum+"", symb);
        //print field 
        System.out.println("");
        System.out.println(cells.get("1") + " | " + cells.get("2") + " | " + cells.get("3"));
        System.out.println("----------");
        System.out.println(cells.get("4") + " | " + cells.get("5") + " | " + cells.get("6"));
        System.out.println("----------");
        System.out.println(cells.get("7") + " | " + cells.get("8") + " | " + cells.get("9"));
        System.out.println("");
    }
    
    public void winChecker(HashMap cells){
        if (cells.get("1").equals(cells.get("2")) && cells.get("1").equals(cells.get("3"))) { //1=2 and 1=3
            System.out.println("Win");    
        } else if (cells.get("4").equals(cells.get("5")) && cells.get("4").equals(cells.get("6"))){//4=5 and 4=6
            System.out.println("Win");    
        } else if (cells.get("7").equals(cells.get("8")) && cells.get("7").equals(cells.get("9"))){//7=8 and 7=9
            System.out.println("Win");    
        } else if (cells.get("1").equals(cells.get("4")) && cells.get("1").equals(cells.get("7"))){//1=4 and 1=7
            System.out.println("Win");    
        } else if (cells.get("2").equals(cells.get("5")) && cells.get("2").equals(cells.get("8"))){//2=5 and 2=8
            System.out.println("Win");    
        } else if (cells.get("3").equals(cells.get("6")) && cells.get("3").equals(cells.get("9"))){//3=6 and 3=9
            System.out.println("Win");    
        } else if (cells.get("1").equals(cells.get("5")) && cells.get("1").equals(cells.get("9"))){//1=5 and 1=9
            System.out.println("Win");    
        } else if (cells.get("3").equals(cells.get("5")) && cells.get("3").equals(cells.get("7"))){//3=7 and 3=7
            System.out.println("Win");    
        }
    }
}
