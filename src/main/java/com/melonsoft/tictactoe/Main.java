/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.melonsoft.tictactoe;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.concurrent.ThreadLocalRandom;
import org.apache.log4j.Logger;

/**
 *
 * @author ArbuzovA
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    final static Logger logger = Logger.getLogger(Main.class);

    public static void main(String[] args) {
        // TODO code application logic here
        //logs a debug message
        try {
            logger.debug("");
            logger.debug("");
            logger.debug("New game started");

            //Variables
            int move = 0;
            String symb;
            Field field = new Field();
            HashMap cells = new HashMapInit().InitMap();
            String avalCells = "123456789";

            //Who is players
            //First player
            //System.out.println("Введите имя первого игрока");
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            String playerFirst = "Player";
            System.out.println("Имя первого игрока : " + playerFirst);

            //Second player
            System.out.println("Введите имя второго игрока");
            String playerSecond = "SkyNet";
            System.out.println("Имя второго игрока : " + playerSecond);

            logger.debug("Players Names defined ");
            logger.debug("Draw");
            System.out.println("Жеребьёвка. Нажмите клавишу → \"ENTER\" для запуска");
            try {
                System.in.read();
            } catch (Exception e) {
            }

            int plDraw = playersDraw();
            if (plDraw >= 51) {
                System.out.println("Первым ходит : " + playerFirst);
            } else {
                System.out.println("Первым ходит : " + playerSecond);
            }
            logger.debug("Start main loop ");

            for (int i = 1; i > 0; i++) {
                //First move
                System.out.println("Ход номер : " + i);
                symb = "X";
                if (plDraw >= 51) {
                    //Player move
                    System.out.println("Ходит : " + playerFirst + " " + symb);
                    System.out.println("Выберите номер ячейки : ");
                    for (int j = 0; j < 10; j++) {
                        move = Integer.parseInt(reader.readLine());
                        if (avalCells.contains(move + "")) {
                            avalCells = avalCells.replace(move + "", "z");
                            //Show field
                            field.ShowField(move, symb, cells);
                            break;
                        } else {
                            System.out.println("Ошибка! Неправильный номер ячейки! Повторите попытку");
                        }
                    }
                } else {
                    //SkyNet move
                    System.out.println("Ходит : " + playerSecond + " '" + symb + "'");
                    //If skyNet is first set cell 5
                    if (i == 1) {
                        move = 5;
                        avalCells = avalCells.replace(move + "", "z");
                        //Show field
                        field.ShowField(move, symb, cells);
                        System.out.println(playerSecond + " занял ячейку " + move + "");
                    }
                }
                
                if (avalCells.equals("zzzzzzzzz")) {
                    System.out.println("Все ячейки заняты");
                    break;
                }

                //Second move
                System.out.println("Ход номер : " + i);
                symb = "O";
                if (plDraw < 51) {
                    System.out.println("Ходит : " + playerFirst + " " + symb);
                    System.out.println("Выберите номер ячейки : ");
                    for (int j = 0; j < 10; j++) {
                        move = Integer.parseInt(reader.readLine());
                        if (avalCells.contains(move + "")) {
                            avalCells = avalCells.replace(move + "", "z");
                            //Show field
                            field.ShowField(move, symb, cells);
                            break;
                        } else {
                            System.out.println("Ошибка! Неправильный номер ячейки! Повторите попытку");
                        }
                    }
                } else {
                    System.out.println("Ходит : " + playerSecond + " " + symb);
                }

                /*
                


                System.out.println("Выберите номер ячейки : ");
                for (int j = 0; j < 10; j++) {
                    move = Integer.parseInt(reader.readLine());
                    if (avalCells.contains(move + "")) {
                        avalCells = avalCells.replace(move + "", "z");
                        //Show field
                        field.ShowField(move, symb, cells);
                        break;
                    } else {
                        System.out.println("Ошибка! Неправильный номер ячейки! Повторите попытку");
                        //Show field
                    }
                }
                 */
            }
        } catch (Exception ex) {
            logger.debug("Main class error " + ex.getLocalizedMessage());
        }
    }

    static int playersDraw() {

        int randomInt = ThreadLocalRandom.current().nextInt(1, 50);
        System.out.println("Random number generated is : " + randomInt);
        return randomInt;
    }
}
