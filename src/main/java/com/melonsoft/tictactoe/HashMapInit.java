/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.melonsoft.tictactoe;

import java.util.HashMap;

/**
 *
 * @author ArbuzovA
 */
public class HashMapInit {
    public HashMap InitMap() {
        HashMap<String, String> cells = new HashMap<>();
        cells.put("1", "1");
        cells.put("2", "2");
        cells.put("3", "3");
        cells.put("4", "4");
        cells.put("5", "5");
        cells.put("6", "6");
        cells.put("7", "7");
        cells.put("8", "8");
        cells.put("9", "9");
        return cells;
    }
}
